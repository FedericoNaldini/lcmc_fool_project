package ast;

import lib.FOOLlib;

public class AndNode implements Node {

	 private Node left;
	 private Node right;
	  
	  public AndNode (Node l, Node r) {
	   left=l;
	   right=r;
	  }
	  
	  public String toPrint(String s) {
	   return s+"Or\n" + left.toPrint(s+"  ")   
	                      + right.toPrint(s+"  ") ; 
	  }
	  
	  /**
	   * Nel typecheck si controlla che il typecheck dei due sottoalberi annidati nei nodi sinistro e destro restituiscano due tipi boolenai,
	   * se ci� risulta vero, allora si restituisce un BoolTypeNode, in caso contrario si termina l'esecuzione del programma
	   */ 
	  public Node typeCheck() {
		Node l= left.typeCheck();  
		Node r= right.typeCheck();  
	    if ( !(FOOLlib.isSubtype(l, new BoolTypeNode()) || FOOLlib.isSubtype(r, new BoolTypeNode())) ) {
	      System.out.println("Incompatible types in AND");
		  System.exit(0);	
	    }  
	    return new BoolTypeNode();
	  }
	  
	  /**
	   * Il risultato delle due generazioni di codice dei nodi di destra e sinistra sono due espressioni booleane,codificate come 0 e 1,
	   * per soddisfare la condizione di and devono essere vere entrambe, di conseguenza la somma dei risultati delle espressioni
	   * deve essere uguale a 2
	   * */
	  public String codeGeneration() {
		  String l1 = FOOLlib.getFreshLabel();
		  String l2 = FOOLlib.getFreshLabel();
		  return "push 2\n" + //metto uno sulla cima dello stack 
				  left.codeGeneration() + right.codeGeneration() + 
				  "add\n" + //sommo i due valori sulla cima dello stack
				  "beq " + l1 + "\n" + //se la somma dei due valori � maggiore o uguale di 1 allora faccio il salto
				  "push 0\n" +  //caso false
				  "b " + l2 + "\n" +
				   l1 + ": \n" + // caso true
				  "push 1\n" +
				   l2 + ":\n";
		  }

}
