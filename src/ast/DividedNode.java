package ast;

import lib.FOOLlib;

public class DividedNode implements Node {

	private Node left;
	private Node right;
	
	
	public DividedNode(final Node left, final Node right) {
		this.left=left;
		this.right=right;
	}

	@Override
	public String toPrint(String s) {
		return s+"Divided\n" + left.toPrint(s+"  ")  
        + right.toPrint(s+"  ") ; 
	}

	/**
	 * L'implementazione del metodo typecheck � analoga a quella della classe MultNode
	 */
	@Override
	public Node typeCheck() {
		if ( ! ( FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) &&
			    FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()) ) ) {
				  System.out.println("Non integers in division");
				  System.exit(0);	
	   }
	   return new IntTypeNode();
	}

	/**
	 * L'implementazione del metodo typecheck � analoga a quella della classe MultNode, l'unica differenza � l'utilizzo della stringa
	 * div al posto della stringa mult
	 */
	@Override
	public String codeGeneration() {
		return left.codeGeneration() + right.codeGeneration() +"div\n";
	}

}
