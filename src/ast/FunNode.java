package ast;
import java.util.ArrayList;

import lib.FOOLlib;

public class FunNode implements Node {

  private String id;
  private Node type; 
  private ArrayList<Node> parlist = new ArrayList<Node>(); // campo "parlist" che � lista di Node
  private ArrayList<Node> declist = new ArrayList<Node>(); 
  private Node exp;
  
  public FunNode (String i, Node t) {
   id=i;
   type=t;
  }
  
  public void addDec (ArrayList<Node> d) {
    declist=d;
  }  

  public void addBody (Node b) {
	exp=b;
  }  

  public void addPar (Node p) { //metodo "addPar" che aggiunge un nodo a campo "parlist"
   parlist.add(p);  
  }  
  
  public String toPrint(String s) {
		 String parlstr="";
		 for (Node par:parlist){parlstr+=par.toPrint(s+"  ");};
		 String declstr="";
		 for (Node dec:declist){declstr+=dec.toPrint(s+"  ");};
	   return s+"Fun:" + id +"\n"
			   +type.toPrint(s+"  ")
			   +parlstr
			   +declstr
               +exp.toPrint(s+"  ") ; 
  }
  
  public Node typeCheck() {
	  for (Node dec:declist){dec.typeCheck();};
      if (! FOOLlib.isSubtype(exp.typeCheck(),type)) {
			  System.out.println("Incompatible value for variable");
			  System.exit(0);
	  }
      return null;
  }
	     
  public String codeGeneration() {
	  String l1 = FOOLlib.getFreshFunction();
	  String declCode = ""; //aggiunge return address fittizio per bilanciare gli AR delle chiamate di funzione
	  String mielPops = "";
	  String parsPops = "";
	  for(Node n : declist) {
		  declCode+= n.codeGeneration();
		  mielPops += "pop\n";
	  }
	  for(Node n : parlist) {
		  parsPops += "pop\n";
	  }
	  FOOLlib.putCode(l1+":\n" + 
	  "cfp\n" + //setta fp all'activation record
	  "lra\n" + //prende Return Address e lo inserisce 	 
	  declCode +
	  exp.codeGeneration() + //genera corpo della funzione
	  "srv\n" + //salva valore di ritorno(pop) della funzione in un registro RV
	  mielPops + //stacca tutte le dichiarazioni dalla cima
	  "sra\n" + //pop return address e memorizzazione in RA
	  "pop\n" + //distrugge access Link
	  parsPops + //stacca tutti i parametri(parametro != dichiarazione)
	  "sfp\n" + //metto contenuto(pop) CL in FP
	  "lrv\n" + //risultato della funzione pushato sullo stack
	  "lra\n" + // metto valore di RA sullo stack
	  "js\n" // faccio la pop e salto
	  );
	  return "push "+ l1 + "\n" +
  
  "";
	  }

}  