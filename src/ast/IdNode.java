package ast;

public class IdNode implements Node {

  private String id;
  private STentry entry;
  private int nestingLevel;
  
  public IdNode (String i, STentry st) {
   id=i;
   entry=st;
  }
  
  public IdNode (String i, STentry st, int nestingLevel) {
	   id=i;
	   entry=st;
	   this.nestingLevel = nestingLevel;
	  }
  
  public String toPrint(String s) {
	   return s+"Id:" + id +"\n" + 
              entry.toPrint(s+"  ") +
              s + "Current nesting level: " + this.nestingLevel + "\n";  
  }

  public Node typeCheck() {
	if (entry.getType() instanceof ArrowTypeNode) {
	  System.out.println("Wrong usage of function identifier");
	  System.exit(0);
	} 
	return entry.getType();
  }
  
  public String codeGeneration() {
	  int diff = this.nestingLevel - entry.getNestingLevel(); // differenza tra posizione attuale e dichiarazione
	  String jump = "";
	  while(diff > 0) {
		  jump = jump + "lw\n";
		  diff--;
	  }
	  
	  return "push " + entry.getOffSet()+"\n" + 
			  "lfp\n" + //scrive sullo stack il valore di fp
			  jump + //risalgo catena statica fino a indirizzo dell'AR in cui � dichiarata la variabile			  
			  "add\n" +
			  "lw\n"; //carica sullo stack ci� che c'� a quell'indirizzo.
	 
	  }

}  