package ast;

import lib.FOOLlib;

public class LessEqualNode implements Node {

	private Node left;
	private Node right;
	
	public LessEqualNode(final Node left, final Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String s) {
		 return s+"Equal\n" + left.toPrint(s+"  ")   
         + right.toPrint(s+"  ") ; 
	}

	/**
	 * Il typeCheck � equivalente a quello di EqualNode.
	 */
	@Override
	public Node typeCheck() {
		Node l= left.typeCheck();  
		Node r= right.typeCheck();  
	    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) {
	      System.out.println("Incompatible types in LessEqual");
		  System.exit(0);	
	    }  
	    return new BoolTypeNode();
	}

	/**
	 * La code generation � praticamente identica a quella di EqualNode, l'unica differenza sta nell'utilizzo di bleq al posto
	 * di beq.
	 */
	@Override
	public String codeGeneration() {
		 String l1 = FOOLlib.getFreshLabel();
		  String l2 = FOOLlib.getFreshLabel();
		  return left.codeGeneration() + right.codeGeneration() + 
				  "bleq " + l1 + "\n" +
				  "push 0\n" +  //caso false
				  "b " + l2 + "\n" +
				   l1 + ": \n" + // caso true
				  "push 1\n" +
				   l2 + ":\n";
	}

}
