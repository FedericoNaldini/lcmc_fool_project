package ast;

import lib.FOOLlib;

public class MinusNode implements Node {
	
	private Node left;
	private Node right;

	public MinusNode(final Node left, final Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String s) {
		 return s+"Minus\n" + left.toPrint(s+"  ")  
         + right.toPrint(s+"  ") ; 
	}

	/**
	 * L'implementazione del metodo typecheck � analoga a quella della classe PlusNode
	 */
	@Override
	public Node typeCheck() {
		if ( ! ( FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) &&
		         FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()) ) ) {
			System.out.println("Non integers in subtraction");
			System.exit(0);	
		}
		return new IntTypeNode();
	}

	/**
	 * L'implementazione del metodo CodeGeneration � analoga a quella della classe PlusNode, con l'unica differenza di aggiungere
	 * la stringa sub al posto della stringa add
	 */
	@Override
	public String codeGeneration() {
		 return left.codeGeneration() + right.codeGeneration() +"sub\n";
	}

}
