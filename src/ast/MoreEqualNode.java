package ast;

import lib.FOOLlib;

public class MoreEqualNode implements Node {

	private Node left;
	private Node right;
	
	public MoreEqualNode(final Node left, final Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String s) {
		 return s+"Equal\n" + left.toPrint(s+"  ")   
         + right.toPrint(s+"  ") ; 
	}

	/**
	 * Il typeCheck � equivalente a quello di EqualNode.
	 */
	@Override
	public Node typeCheck() {
		Node l= left.typeCheck();  
		Node r= right.typeCheck();  
	    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) {
	      System.out.println("Incompatible types in MoreEqual");
		  System.exit(0);	
	    }  
	    return new BoolTypeNode();
	}

	/**
	 * La code generation � praticamente identica a quella di LessEqualNode, l'unica differenza sta nell'invertire 
	 * l'ordine degli argomenti della condizione.
	 */
	@Override
	public String codeGeneration() {
		 String l1 = FOOLlib.getFreshLabel();
		  String l2 = FOOLlib.getFreshLabel();
		  return right.codeGeneration() + left.codeGeneration() + 
				  "bleq " + l1 + "\n" +
				  "push 0\n" +  //caso false
				  "b " + l2 + "\n" +
				   l1 + ": \n" + // caso true
				  "push 1\n" +
				   l2 + ":\n";
	}

}
