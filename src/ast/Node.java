package ast;
public interface Node {
    
  String toPrint(String indent);

//fa il type checking e ritorna: 
//per una espressione, il suo tipo (oggetto BoolTypeNode o IntTypeNode)
//per una dichiarazione, "null"
  Node typeCheck(); 
 
  //Genera la stringa del codice assembler
  String codeGeneration();
}  