package ast;

import lib.FOOLlib;

public class NotNode implements Node {

	private Node exp;
	
	public NotNode(final Node exp) {
		this.exp = exp;
	}

	@Override
	public String toPrint(String s) {
		return s+"Not\n" + exp.toPrint(s+"  ");
	}

	/**
	 * Come nel linguaggio C, la negazione di un valore >= 1 conduce a zero, mentre la negazione di 0 a 1, di conseguenza
	 * si controlla che il typecheck di expression produca un IntTypeNode, ci� che l'espressione genera � un BoolTypeNode
	 */
	@Override
	public Node typeCheck() {
		 if ( ! ( FOOLlib.isSubtype(exp.typeCheck(), new BoolTypeNode()))) {
					  System.out.println("Non Boolean in Negations");
					  System.exit(0);	
		   }
		   return new BoolTypeNode();
	}

	/**Calcolo del valore dell'espressione da negare con CodeGenerations, dopodich� viene pushato uno 0 sullo stack e si 
	 * esegue un'istruzione di confronto, se i valori sono entrambi 0 allora si pusha 1 come risultato dell'operazione, in caso contrario
	 * si inserisce 0
	 * */
	@Override
	public String codeGeneration() {
		String l1 = FOOLlib.getFreshLabel();
		  String l2 = FOOLlib.getFreshLabel();
		  return exp.codeGeneration() + 
				  "push 0\n" +
				  "beq " + l1 + "\n" +
				  "push 0\n" +  //caso false
				  "b " + l2 + "\n" +
				   l1 + ": \n" + // caso true
				  "push 1\n" +
				   l2 + ":\n";
	}

}
