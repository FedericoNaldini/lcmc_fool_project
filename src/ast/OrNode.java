package ast;

import lib.FOOLlib;

public class OrNode implements Node {

	 private Node left;
	 private Node right;
	  
	  public OrNode (Node l, Node r) {
	   left=l;
	   right=r;
	  }
	  
	  public String toPrint(String s) {
	   return s+"Or\n" + left.toPrint(s+"  ")   
	                      + right.toPrint(s+"  ") ; 
	  }
	
	  /**
	   * Nel typecheck si controlla che il typecheck dei due sottoalberi annidati nei nodi sinistro e destro restituiscano due tipi boolenai,
	   * se ci� risulta vero, allora si restituisce un BoolTypeNode, in caso contrario si termina l'esecuzione del programma
	   */
	  public Node typeCheck() {
		Node l= left.typeCheck();  
		Node r= right.typeCheck();  
		if ( !(FOOLlib.isSubtype(l, new BoolTypeNode()) || FOOLlib.isSubtype(r, new BoolTypeNode())) ) {
	      System.out.println("Incompatible types in Or");
		  System.exit(0);	
	    }  
	    return new BoolTypeNode();
	  }
	  

/**
 * L'idea della Code Generations � la seguente: viene pushato un uno sullo stack, dopodich� viene generato il codice per le due espressioni
 * di sinistra e destra dell'or, entrambe mettono sulla pila un espressione booleana codificata come uno zero oppure un uno.
 * Eseguendo la somma del primo e secondo valore sulla cima dello stack ottengo un valore maggiore o uguale di uno nel caso almeno una delle
 * due espressioni risultasse vera, di conseguenza � sufficiente utilizzare una operazione beq che controlla che l'uno pushato all'inizio sia
 * minore o uguale del risultato della somma calcolata
 */
	  public String codeGeneration() {
		  String l1 = FOOLlib.getFreshLabel();
		  String l2 = FOOLlib.getFreshLabel();
		  return "push 1\n" + //metto uno sulla cima dello stack 
				  left.codeGeneration() + right.codeGeneration() + 
				  "add\n" + //sommo i due valori sulla cima dello stack
				  "bleq " + l1 + "\n" + //se la somma dei due valori � maggiore o uguale di 1 allora faccio il salto
				  "push 0\n" +  //caso false
				  "b " + l2 + "\n" +
				   l1 + ": \n" + // caso true
				  "push 1\n" +
				   l2 + ":\n";
		  }
}
