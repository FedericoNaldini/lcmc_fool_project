package ast;
import java.util.ArrayList;

import lib.FOOLlib;

public class ProgLetInNode implements Node {

  private ArrayList<Node> declist;
  private Node exp;
  
  public ProgLetInNode (ArrayList<Node> d, Node e) {
   declist=d;
   exp=e;
  }
  
  public String toPrint(String s) {
	 String declstr="";
	 for (Node dec:declist){declstr+=dec.toPrint(s+"  ");};
     return s+"ProgLetIn\n" + declstr + exp.toPrint(s+"  ") ; 
  }

  public Node typeCheck() {
    for (Node dec:declist){dec.typeCheck();};
    return exp.typeCheck();
  }
    
  public String codeGeneration() {
	  String s = "push 0\n "; //aggiunge return address fittizio per bilanciare gli AR delle chiamate di funzione
	  for(Node n : declist) {
		  s+= n.codeGeneration();
	  }
	  return s + exp.codeGeneration()+"halt\n" + FOOLlib.getCode(); //aggiunge in coda le dichiarazioni
	  }

}  