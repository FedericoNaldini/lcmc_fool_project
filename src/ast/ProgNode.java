package ast;

public class ProgNode implements Node {

  private Node exp;
  
  public ProgNode (Node e) {
   exp=e;
  }
  
  public String toPrint(String s) {
    
   return s+"Prog\n" + exp.toPrint(s+"  ") ;
  }
  
  public Node typeCheck() {
	    return exp.typeCheck();
  }
    
  /**
   * Non ho nemmeno ambiente globale in prognode
   */
  public String codeGeneration() {
	  return exp.codeGeneration()+"halt\n"; //alt termina ciclo fetch execute
	  }

}  