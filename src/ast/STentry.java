package ast;
public class STentry {
   
  private int nl;
  private Node type;
  private int offset;
  
  public STentry (int n) {
	  nl=n;
  } 

  public STentry (int n, Node t) {
	  nl=n;
	  type=t;
  } 
  
  public STentry (int n, int offset) {
	  nl=n;
	  this.offset = offset;
  } 
  
  public STentry (int n, Node t, int offset) {
	  nl=n;
	  type=t;
	  this.offset = offset;
  } 
  
  public void addType(Node t) {
	  type=t;
  }

  public Node getType() {
	  return type;
  }
  
  public int getNestingLevel() {
	  return this.nl;
  }
  
  public int getOffSet() {
	  return this.offset;
  }
  
  
  public String toPrint(String s) {
	   return s+"STentry: nestlev " + Integer.toString(nl) +"\n"+
			  s+"STentry: type\n " +
			      type.toPrint(s+"  ") +
			      s+"STentry: offset " + Integer.toString(offset) +"\n";  
  }
  
}  