package lib;

import ast.*;

public class FOOLlib {
	
	private static int labCount = 0; //etichettatore
	private static int funCount = 0; //etichettatore di funzioni
	private static String funCode = "";
  //valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base: int o bool
  public static boolean isSubtype (Node a, Node b) {
	return a.getClass().equals(b.getClass()) ||
	    	   ( (a instanceof BoolTypeNode) && (b instanceof IntTypeNode) );  
  }
  
  public static String getFreshLabel() {
	  return "label" + labCount++;
  }
	
  public static String getFreshFunction() {
	  return "function" + funCount++;
  }
  
  public static void putCode(String c) {
	  funCode += "\n" + c;
  }
  
  public static String getCode() {
	  return funCode;
  }
}
